.DEFAULT_GOAL := help
CONTAINER_NAME?=poe-stash-builder
CONTAINER_DAEMON?=podman

help: ## Displays this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-30s\033[0m %s\n", $$1, $$2}'

image: ./container/* ## Creates an image for the builder
	${CONTAINER_DAEMON} build \
		--file ./container/Containerfile \
		--tag ${CONTAINER_NAME} \
		--build-arg USER_ID=$(id -u) \
		--build-arg GROUP_ID=$(id -g) \
		.

npm: image remove-container ## Starts the container and executes command requested in $arguments via npm
	${CONTAINER_DAEMON} container run \
		--userns=keep-id \
		--name ${CONTAINER_NAME} \
		--rm \
		--tty \
		--interactive \
		--publish 1234:1234 \
		--volume "${CURDIR}":/app/project \
		${CONTAINER_NAME} \
		run $(arguments)

build:
	make npm arguments=build

live:
	make npm arguments=serve

remove-container: ## Removes pre-existing containers with the same name
	@echo "Checking for container named ${CONTAINER_NAME}..."
	@CONTAINER_ID=$(${CONTAINER_DAEMON} ps -aqf "name=${CONTAINER_NAME}"); \
	if [ -n "$${CONTAINER_ID}" ]; then \
		echo "Container '${CONTAINER_NAME}' already exists. Removing it..."; \
		${CONTAINER_DAEMON} container stop $${CONTAINER_ID}; \
		${CONTAINER_DAEMON} container rm $${CONTAINER_ID}; \
	else \
		echo "Container ${CONTAINER_NAME} does not exist yet."; \
	fi

stop: ## Stops running builder
	${CONTAINER_DAEMON} container stop ${CONTAINER_NAME}

clean: remove-container ## Removes all temporary data used by the builder. Stops the builder in the process.

%:
	@:
# ref: https://stackoverflow.com/questions/6273608/how-to-pass-argument-to-makefile-from-command-line