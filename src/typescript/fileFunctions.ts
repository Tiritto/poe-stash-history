import { fileSelector } from "../script";
import { csvToStashLog, stashHistoryIntoBalanceList } from "./stashLogFunctions";

/**
 * Checks if the given `file` is a CSV file.
 */
export function isCSV(file: File) {
    return (file.type === 'text/csv' && file.name.endsWith('.csv'))
}

/**
 * This function gets triggered when a new file is uploaded by the user
 */
export function readFileAsText(file: File): Promise<string> {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
            const result = reader.result;
            if (result !== null && typeof result === 'string') {
                resolve(result);
            } else {
                reject(new Error("Unknown type"));
            }
        };
        reader.onerror = reject;
        reader.readAsText(file);
    });
}