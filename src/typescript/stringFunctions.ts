import { ItemQuantity } from "./dataTypes";

export function stringToDate(string: string) {
    const [date, time] = string.split(", ");
    const [day, month, year] = date.split('.').map(parseInt);
    const [hour, minute, second] = time.split(':').map(parseInt);
    return new Date(year, month, day, hour, minute, second);
}

export function splitItemString(string: string) {
    if (string.includes('× ')) {
        const indexOfSpace = string.indexOf(" ");
        return [ string.substring(0, indexOfSpace - 1), string.substring(indexOfSpace + 1) ];
    }
    return [ "1", string ];
}

export function quantityStringToNumber(quantityString: string): number {
    return parseFloat(quantityString) * (quantityString.includes('K') ? 1000 : 1);
}

export function getLastElementOfLine(csvLine: string) {
    return csvLine.substring(csvLine.lastIndexOf(",") + 1);
}