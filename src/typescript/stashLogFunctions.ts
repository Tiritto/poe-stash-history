import { stringToDate, extractItemData, splitItemString, quantityStringToNumber, getLastElementOfLine } from "./stringFunctions";
import { StashHistory, BalanceList } from "./dataTypes";
import { conversionTable } from "./conversionTable";

/**
 * Converts logs from StashHistory into BalanceList containing data about all players' current balance
 * @param {StashHistory} stashHistory - StashHistory object containing history of all operations on the stash
 * @param league - League for which balance will be genereted
 */
export function stashHistoryIntoBalanceList(stashHistory: StashHistory, league: string) {
    const balance: BalanceList = new Map();

    // Accumulate all operations per item for each player in selected league
    for (const { player, item, change } of stashHistory.filter(e => e.league == league)) {
        balance[player] = {
            ...(balance[player] || {}),
            [item]: (balance[player]?.[item] || 0) + change
        };
    }

    // Some items are considered as fractions of another item (fragments).
    // Convert those fragment items into proportional equivalent of final product.
    for (const player of balance.keys()) {
        for (const [itemName, itemQuantity] of balance[player]) {
            if (itemName in conversionTable) {
                const [multiplier, conversionTarget] = conversionTable[itemName];
                balance[player][conversionTarget] += (balance[player][conversionTarget] ?? 0) + (itemQuantity * multiplier);
                delete balance[player][itemName]; // Remove previous currency after conversion is done
            }
        }
    }

    return balance;
}

/**
 *  Converts contents of a .CSV file into StashLog object that contains history of operations
 *
 *  @param {any} csv - Contents of a CSV file that is supposed to be processed
 *  @param {string} delimeter - Delimeter used by CSV file. Defaults to `,` as it shold it most cases.
 */
export function csvToStashLog(csv: string, delimeter = '","') {
    const stashLog: StashHistory = [];
    let lastKnownAmount = {};

    const lines = csv.split('\n').slice(1).reverse();
    lines.forEach((line, i) => {
        
        const [date, league, player, action, /*stash*/, itemString] = line.trim().slice(1, -1).split(delimeter);
        const [ quantityString, itemName ] = splitItemString(itemString);

        // check if next item is also the same
        const nextItemString = getLastElementOfLine(lines[i + 1]);
        if (nextItemString.includes(itemName)) {
            
        }

        const quantity = quantityStringToNumber(quantityString);

        // Create default values for uninitialized variables
        lastKnownAmount[league] = lastKnownAmount[league] ?? {};
        lastKnownAmount[league][itemName] = lastKnownAmount[league][itemName] ?? 0;

        // Check if following item in the history is the same one
        if (lines[i+1])

        // Calculate the balance outcome and update `lastKnownAmount`
        const balance: number =
            action == "added" ? item.quantity :
            action == "removed" ? -item.quantity :
            item.quantity - lastKnownAmount[league][item.name];
        lastKnownAmount[league][item.name] += balance;

        // Push collected information into stashLog array
        stashLog.push({
            date: stringToDate(date),
            league: league,
            player: player,
            item: item.name,
            change: balance
        });
    });
    return stashLog;
}
