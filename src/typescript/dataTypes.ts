export interface ItemQuantity {
    name: string;
    quantity: number;
}

export interface StashLogEntry {
    date: Date;
    player: string;
    league: string;
    item: string;
    change: number;
}



export type StashHistory = StashLogEntry[];

export type BalanceList = Map<string, Map<string, number>>;

export interface ItemData {
    name: string;
    iconUrl: string;
    chaosEquivalent: number;
}
export type ItemDataList = { [key: string]: ItemData; }