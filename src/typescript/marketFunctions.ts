import { BalanceList, ItemData, ItemDataList } from "./dataTypes";

function handleErrors(response: Response) {
    if (!response.ok) throw Error(response.statusText);
    return response;
}



/**
 * Reads contents of `BalanceList` object and converts items quantity into their respective Chaos Orb equivalents.
 */
export function convertToChaos(balanceList: BalanceList, marketData: ItemDataList) {
    const balanceChaosEquivalent: BalanceList = new Map();
    for (const player in balanceList) {
        balanceChaosEquivalent[player] = new Map();
        for (const item in balanceList[player]) {
            if (item in marketData) {
                balanceChaosEquivalent[player][item] = balanceList[player][item] * marketData[item].chaosEquivalent;
            }
        }
    }
    return balanceChaosEquivalent;
}

function format(data): ItemDataList {
    const itemsDatabase: { [key: string]: ItemData } = {};
    if (data?.lines && data?.currencyDetails?.length) {

        for (const item of data.currencyDetails) {
            if (item.name && item.icon) {
                itemsDatabase[item.name] = {
                    name: item.name,
                    iconUrl: item.icon,
                    chaosEquivalent: 0
                };
            }
        }

        // Update with correct chaosEquivalent values
        for (const item of data.lines) {
            if (item.chaosEquivalent && item.currencyTypeName && item.currencyTypeName in itemsDatabase) {
                itemsDatabase[item.currencyTypeName].chaosEquivalent = item.chaosEquivalent;
            }
        }
    }
    return itemsDatabase;
}

const localMarketData = "./market.json";
function getAPI(league: string) {
    let url = `https://poe.ninja/api/data/currencyoverview?league=${league}&type=Currency`;

    // Use CORS proxy when running from localhost
    if (['localhost', '127.0.0.1', ''].includes(location.hostname)) {
        url = `https://api.codetabs.com/v1/proxy?quest=${url}`;
    }

    return url;
}

export function getMarketData(league: string) {
    return fetch(getAPI(league), { mode: 'cors', method: 'GET' })
        .then(handleErrors)
        .then(response => response.json().then(format))
        .catch(error => {
            console.error(`An error has occured while trying to fetch online 'marketData'. ${error}`);
            console.info("Attempting to load `marketData` from local backup...");
            return fetch(localMarketData, { method: 'GET' })
                .then(handleErrors)
                .then(response => response.json().then(format))
                .catch(error => {
                    const errorMessage = `An error has occured while trying to fetch local 'marketData'. ${error}`;
                    console.error(errorMessage);
                    return Promise.reject(new Error(errorMessage));
                });
        })
}