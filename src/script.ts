// @ts-check
import { BalanceList, ItemDataList, StashHistory } from "./typescript/dataTypes";
import { isCSV, readFileAsText } from "./typescript/fileFunctions";
import { convertToChaos, getMarketData } from "./typescript/marketFunctions";
import { csvToStashLog, stashHistoryIntoBalanceList } from "./typescript/stashLogFunctions";

export const fileSelector = document.getElementById('fileSelector')! as HTMLInputElement;
const leagueSelector = document.getElementById('leagueSelector')! as HTMLSelectElement;

export var chaosEquivalent = {}; // TODO REMOVE
var currencyDetails = {};

function populateData(balanceList) {
    const tables = document.getElementsByTagName('table');

    for(const table of tables) {

        const automatedColumns = Array.from(table.querySelectorAll("td[data-name]"))
            .flatMap(td => {
                const dataName = td.getAttribute('data-name');
                return dataName ? [dataName] : [];
            });
        const tBody = table.createTBody();

        // Insert row for each player
        for(const playerName in balanceList) {
            const tr = tBody.insertRow();
            let totalChaosEquivalent = 0;
            tr.insertCell().textContent = playerName;
            for (const itemName of automatedColumns) {
                const count = balanceList[playerName][itemName] | 0;
                const cell = tr.insertCell();
                cell.textContent = count.toFixed(2);

                // Assign class depending on the balance
                cell.classList.add(count > 0 ? 'positive' : count < 0 ? 'negative' : 'zero');

                // Add chaos equivalent to the total
                totalChaosEquivalent += chaosEquivalent[itemName] * count;
            }
            tr.insertCell().textContent = totalChaosEquivalent.toFixed(2);
        }
    }
}

// Turns on ability to use the input
function enableInput() {
    document.getElementById('apiStatus')!.innerHTML = "";
}

function iconizeHeaderRows() {
    for (const td of document.getElementsByTagName('td')) {
        const attribute = td.getAttribute("data-icon");
        if (attribute && attribute in currencyDetails) {
            const content = td.textContent;
            td.innerHTML = `<img src="${currencyDetails[attribute].icon}" alt="${attribute}" title="${content}">`;
        }
    }
}

// MAIN
const defaultLeague = "Sanctum";
/**
 * Returns name of a league selected by user.
 * TODO: Function is not complete yet. Should get value from <option> selection.
 */
function getSelectedLeague() {
    return leagueSelector.value ?? defaultLeague;
}

/**
 * Analyses StashHistory object searching for all unique league names within it
 * @param stashHistory - StashHistory object that will be used for analysis
 * @returns - Returns an array with names of all leagues present in the StashHistory
 */
function getLeagues(stashHistory: StashHistory) {
    const leagues: string[] = [];
    for (const entry of stashHistory) {
        if (!leagues.includes(entry.league)) {
            leagues.push(entry.league);
        }
    }
    return leagues;
}

function onLeagueSelection(stashHistory: StashHistory, marketData: ItemDataList) {
    const balanceList = stashHistoryIntoBalanceList(stashHistory, getSelectedLeague());
    const balanceListInChaos = convertToChaos(balanceList, marketData);

    console.log(balanceList);

    fillTables(balanceList, balanceListInChaos);
}

function fillTables(balanceList: BalanceList, balanceListInChaos: BalanceList) {
    for (const table of document.querySelectorAll('table.automatic') as NodeListOf<HTMLTableElement>) {

        // Get thead and ignore the table if for whatever reason it doesn't exist
        const thead = table.querySelector('thead');
        if (!thead) {
            console.error("<thead> doesn't exist for iterated table! Skipping...");
            continue;
        }

        // Create tBody if it doesn't exist yet or use already existing one
        const tbody = table.querySelector('tbody') || table.createTBody();
        tbody.textContent = "";

        // const automatedColumnsx = Array.from(tbody.querySelectorAll("td[data-name]"))
        //     .map(td => td.getAttribute("data-name"))
        //     .filter(dataName => dataName !== null);
        // console.log(automatedColumnsx);

        const automatedColumns = Array.from(thead.querySelectorAll("td[data-name]"))
            .flatMap(td => {
                const dataName = td.getAttribute('data-name');
                console.log(`Processing column ${dataName}`);
                return dataName ? [dataName] : [];
            });

        // For each player
        for (const player in balanceList) {
            const tr = tbody.insertRow();
            tr.insertCell().textContent = player;

            // First column will always include name of a plyer
            for (const columnItem of automatedColumns) {
                console.log(`Processing over ${columnItem}`);
                const count = balanceList[player][columnItem] | 0;
                const chaosEquivalent = balanceListInChaos[player][columnItem] | 0;
                const cell = tr.insertCell();

                const rawCount = document.createElement('div');
                rawCount.textContent = count.toFixed(2);
                cell.appendChild(rawCount);

                const chaosCount = document.createElement('div');
                chaosCount.classList.add('chaos');
                chaosCount.textContent = chaosEquivalent.toFixed(2);
                cell.appendChild(chaosCount);

                // Assign class depending on the balance
                cell.classList.add(count > 0 ? 'positive' : count < 0 ? 'negative' : 'zero');

                // // Add chaos equivalent to the total
                // totalChaosEquivalent += chaosEquivalent[columnItem] * count;
                // tr.insertCell().textContent
            }

            // For each column

        }
    }
}

getMarketData(defaultLeague).then(marketData => {
    if (!marketData) alert("Empty marketdata. Failed.");

    // Once marketData has been loaded, allow user to upload CSV file
    fileSelector.addEventListener('change', () => {
        const files = fileSelector.files ?? [];
        if (files.length > 0 && isCSV(files[0])) {
            readFileAsText(files[0]).then(fileContent => {

                // Convert contents of CSV file into StashHistory object
                const stashHistory = csvToStashLog(fileContent);

                // List all leagues from the StashHistory as options
                leagueSelector.innerText = ""; // Clears previous options
                for (const league of getLeagues(stashHistory)) {
                    leagueSelector.add(new Option(league));
                }
                leagueSelector.disabled = false;

                // Whenever league is selected or changed, update displayed data
                leagueSelector.addEventListener("change", () => onLeagueSelection(stashHistory, marketData));
            })
        }
    });
    fileSelector.disabled = false;

    iconizeHeaderRows(); //TODO: ???
}).catch(err => {
    alert("Failed to load marketplace data. Aborting...");
});