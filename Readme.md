# **This project was abandoned before it was completed and is no longer being developed!** 

While working on this project, I came across several design errors on the part of GGG, which prevent further development of this project, because the data obtained from GGG are not reliable and credible.

If you would like to know more about the problems that have occurred, I have already reported them to the Path of Exile forum, informing about the problem and suggesting few possible solutions.

[**Click here to view the thread:** Guild stash logs are simply unreliable](https://www.pathofexile.com/forum/view-thread/3345871)

Until this issue is resolved, I see no reason to continue working on this project.

# PoE Guild Stash History

Simple app that reads guild stash history from a CSV file and displays in-depth statistics regarding the biggest contributors and beneficiaries of the guild stash.

## Getting Started

### Dependencies

* GNU Make for `make` calls. I've used GNU Make 4.3.
* Podman 4.3.1 for rootless and daemon-less containerization.
  * Docker *should* work as drop-in replacement for `podman`, but I've never tested it.

### Installing & executing

* Use `make live` to generate live preview of the project.
* Use `make build` to compile into static files for the web located in `/dist`.
* Use `make clean` to clean any containerization and building leftovers.

## License

This project is licensed under the WTFPL License - see the LICENSE.md file for details

## Acknowledgments

* [poe.ninja](https://poe.ninja/) - Source of data regarding current market data.
* [CodeTabs](https://codetabs.com/) - CORS Proxy that was really useful during localhost testing.